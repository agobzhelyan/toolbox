<?php
require_once(__DIR__.'/functions.php');
$user = trim(`whoami`);
$workingDir = getcwd();
if(empty($argv[1])) {
	writeln('Usage: sudo toolbox addhost <hostname>');
	die;
}

requireRootUser();
// ask for host name or get from paramter
$host = $argv[1];
// check apache virtual hosts
writeln('Looking for existing vhosts with this name...');
$hostShellEscaped = escapeshellarg($host);
$searchCommand = "grep -r $hostShellEscaped /etc/apache2/sites-enabled/";
$searchResults =  trim(`$searchCommand`);
if ('' === $searchResults) {
	writeln('    nothing found.');
} else {
	writeln('Some virtual hosts already contain this string:');
	writeln($searchResults);
	if ( 'n' ===strtolower(prompt('Continue anyway? Y/n [Y]')) ) {
		die;
	}
}
// check /etc/hosts
writeln('Looking for existing records in /etc/hosts with this name...');
$searchCommand = "cat /etc/hosts |grep $hostShellEscaped";
$searchResults =  trim(`$searchCommand`);
if ('' === $searchResults) {
	writeln('    nothing found.');
} else {
	writeln('Some /etc/hosts records already contain this string:');
	writeln($searchResults);
	if ( 'n' ===strtolower(prompt('Continue anyway? Y/n [Y]')) ) {
		die;
	}
}

// ask for the docroot
$defaultDocroot = getcwd();
$answer = prompt(sprintf("What's the docroot? [$defaultDocroot]"));
$docroot = (''===trim($answer)) ? $defaultDocroot : $answer;

// ask for IP
$defaultIp = '127.0.0.1';
$answer = prompt(sprintf("What's the IP your apache is running at? [$defaultIp]"));
$ip = (''===trim($answer)) ? $defaultIp : $answer;

$vhostTpl = getDefaultVhostTempate();
$parsedVhostTemplate = parseVhostTemplate($vhostTpl, array(
	'%docroot%' => $docroot,
	'%hostname%' => $host
));
writeln('We come up with virtual host like the one below:');
writeln('');
writeln($parsedVhostTemplate);
$answer = prompt(sprintf("Do you want to edit it (y/N)? [n]"));
if (strtolower($answer)==='y') {
	$tmpPath = tempnam(sys_get_temp_dir(), 'instalr_');
	file_put_contents($tmpPath, $parsedVhostTemplate);
	// todo : select editor, work in ssh shell
	`gedit $tmpPath`;
	$parsedVhostTemplate = file_get_contents($tmpPath);
	unlink($tmpPath);
}
writeln("Ok, we're going to \n  - create new virtual host with docroot at $docroot\n  - add new record to /etc/hosts ($ip $host)\n  - and restart apache, ok?");
$answer = prompt(sprintf("So, let's do it? (Y/n)? [Y]"));
if ('n' === strtolower($answer)) {
	writeln('Well, as you wish.');
	die;
}
$vhostFile = $host;
file_put_contents('/etc/apache2/sites-available/'.$vhostFile, $parsedVhostTemplate);
echo `a2ensite $vhostFile`;
echo `apache2ctl restart`;
file_put_contents('/etc/hosts', file_get_contents('/etc/hosts')."\n$ip    $host");
writeln('Done.');

function parseVhostTemplate($tpl, $data) {
	$tpl = strtr($tpl, $data);
	$tpl = preg_replace('/^##.*$/m', '', $tpl);
	return trim($tpl);
}

function getDefaultVhostTempate() {
$tpl = <<<TPL
## Note:
##  - %hostname% and %docroot% will be replaced with the values you've entered
##  - comments starting with double hash mark - will be removed
# Auto-generated with installr script
<VirtualHost *:80>
    ServerName %hostname%
    ServerAlias www.%hostname%
    DocumentRoot %docroot%
    <Directory %docroot%>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
    </Directory>
</VirtualHost>
TPL;
return $tpl;
}

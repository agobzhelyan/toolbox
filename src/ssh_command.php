<?
//todo: make it remember login / pass
//$debug = true;
require_once(__DIR__.'/functions.php');
$urlRoot = 'http://extranet.insign.ch/icmsadmin/';

function debug($s)
{
	global $debug;
	if (!$debug) return;
	echo($s);
}

function getHeading($cnt)
{
	$res = '';
	if (preg_match('%<h2>(.*?)</h2>%is', $cnt, $m))
	{
		$res = trim(strip_tags($m[1]));
	}
	return $res;
}

function queryUrl($url, $fields, $method='GET')
{
	$sesName = 'SID';
	static $SID;
	$urlstring = '';
	foreach($fields as $key => $value)
	{
		 $value = urlencode($value);
		 $key = urlencode($key);
		 $urlstring .= "&" . $key . "=" . $value;
	}
	if ($method=='GET')
	{
		$url.="?$urlstring";
	}
	$ch = curl_init($url);

	if ($method == 'POST')
	{
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $urlstring);
	}
	
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	
	//curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Forwarded-For: '.$_SERVER['REMOTE_ADDR']));
	
	if ($SID)
	{
		curl_setopt($ch, CURLOPT_COOKIE, $sesName.'='.$SID);
	}
	$data = curl_exec($ch);
	preg_match('/^(.*?)(?:(?:\r\n){2}|\r{2}|\n{2})(.*)$/s', $data, $m);
	$data = $m[2];
	$headers = $m[1];
	if (preg_match_all('/^Set-Cookie.*?$/mi', $headers, $m))
	{		
		foreach($m[0] as $cookieHeader)
		{
			//header($cookieHeader);//forward cookies from requested page
			//echo "'$cookieHeader' \n";			
			if (preg_match('/[a-f0-9]{32}/', $cookieHeader, $cookieHash))
			{
				$SID = $cookieHash;
			}
		}
		//detect session cookie and set to static var $SID
		
	}
	$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	if ($code>400) debug("Got $code response\n");
	curl_close($ch);
	return $data;
}

function getUserListing($cnt)
{
	if (!preg_match('%<select name="objects\.user"[^>]*>(.+?)</select>%s', $cnt, $m))
	{
		return '';	
	}
	$res = "";
	$list = $m[1];
	preg_match_all('%<option value="(.*?)">(?:&nbsp;)*\s?(.*?)</option>%s', $list, $m);
	$namesByLine = 4;
	$namesInLine = 0;
	foreach($m[0] as $k=>$v)
	{
		if ($m[1][$k]=="0") continue;
		elseif (strpos($m[1][$k], 'group_')===0)
		{
			$res.="\n== ".$m[2][$k]." ==\n";
			$namesInLine = 0;
		}
		else
		{
			$namesInLine++;
			if ($namesInLine>$namesByLine)
			{
				$res.="\n";
				$namesInLine=1;
			}
			$res.=$m[2][$k]."\t";
			
		}
	}
	return $res;	
}

if (empty($argv[1])) die("No site name given\nUsage:\ntoolbox ssh ownername.ch\n");
$site = $argv[1];

$cnt = queryUrl($urlRoot.'icms/access/index.html', array());
$loginScreenFound = false;
$presets = loadPreset('ssh', 'default');
while(strpos($cnt, 'personalizer.login') && strpos($cnt, 'personalizer.pw'))
{
	$loginScreenFound = true;
	$head = getHeading($cnt);
	debug("got login screen \n -=$head=- \n");
	$defaultLogin = empty($presets['suLogin']) ? null : $presets['suLogin'];
	$login = prompt('superuser login'.($defaultLogin ? sprintf(' [%s]', $defaultLogin) : ''));
	if (!$login) $login = $defaultLogin;
	if (!$login) die("aborted\n");
	$presets['suLogin'] = $login;
	savePreset('ssh', 'default', $presets);
	$pass = prompt('superuser pass', true);
	echo "\n";
	$params = array();
	$params['personalizer.action'] = 'lp_checkPW';
	$params['personalizer.login'] = $login;
	$params['personalizer.pw'] = $pass;
	$cnt = queryUrl($urlRoot.'icms/login.html', $params, 'POST');	
}
if (!$loginScreenFound)
{
	die("Didn't get login screen expected at {$urlRoot}icms/access/index.html. Aborting.\n");	
}
$cnt = queryUrl($urlRoot.'icms/access/index.html', array());
$head = getHeading($cnt);
debug("login screen passed \n -=$head=-\n");

$defaultAdminLogin = empty($presets['adminLogin']) ? null : $presets['adminLogin'];
do
{
	$login = prompt('your icmsadmin user login (or type !list to get listing)'.($defaultAdminLogin ? sprintf(' [%s]', $defaultAdminLogin) : ''));
	if (!$login) $login = $defaultAdminLogin;
	if (!$login) die("aborted\n");
	$presets['adminLogin'] = $login;
	savePreset('ssh', 'default', $presets);
	if (preg_match('%<option value="(\d+)">(&nbsp;)* '.preg_quote($login).'</option>%is', $cnt, $m))
	{
		$userId = $m[1]	;
		break;
	}
	else
	{
		if ($login == '!list')
		{
			echo getUserListing($cnt)."\n";
		}
		else
		{
			echo "Can not find uid for user '$login'. \n";
		}
	}
}
while (true);
if (preg_match('%<option value="(\d+)">'.preg_quote($site).'</option>%is', $cnt, $m))
{
	$siteId = $m[1];
}
else
{
	die("Can not find ID for site '$site'. Aborting.\n");
}
$params = array();
$params['objects.user'] = $userId;
$params['objects.right'] = 1;
$cnt = queryUrl($urlRoot.'icms/access/index.html', $params);
preg_match_all('%<a href="info\.html\?objects\.search=(.*?)"%is', $cnt, $m);
$haveAccess = false;
if (is_array($m[1]))
{
	if(in_array($site, $m[1]))
	{
		$haveAccess = true;	
	}
}

$cnt2 = queryUrl($urlRoot.'icms/access/info.html', array('objects.search'=>$site));
$head = getHeading($cnt2);
debug("-=$head=-\n");
preg_match_all('%Details for Domain (.+?) on (.+?)\'.*?ssh://(.+?)"%si', $cnt2, $m);
$sites = array();
$accaunts = array();
if (is_array($m[0]) && count($m[0]))
{
	debug("search results for '$site':\n");
	foreach($m[1] as $k=>$void)
	{
		if ($m[1][$k] == $site)
		{
			$val = $m[1][$k].'@'.$m[2][$k];
			$sites[count($sites)+1] = $val;
			$accaunt = $m[3][$k];
			$accaunts[count($sites)] = $accaunt;
			echo count($sites)."\t$val \t( $accaunt )\n";
		}
	}
}
else
{
	die("No info found about site '$site'. Aborting.\n");	
}

if (!$haveAccess)
{	
	$resp = strtolower(prompt("you have no access to $site. Add access? (y/n)"));
	if ($resp != 'y') die("ok, bye\n");
	$params = array('objects.action'=>'saveRight', 'objects.owner'=>$siteId, 'objects.user'=>$userId, 'objects.right'=>1);
	$cnt = queryUrl($urlRoot.'icms/access/index.html', $params);
	$head = getHeading($cnt);
	debug("-=$head=-\n");
//debug($cnt);
	//check if access was added
	preg_match_all('%<a href="info\.html\?objects\.search=(.*?)"%is', $cnt, $m);
	$haveAccess = false;
	if (is_array($m[1]))
	{
		if(in_array($site, $m[1]))
		{
			$haveAccess = true;	
		}
	}
	if ($haveAccess)
	{
		echo "SSH access for you on '$site' was successfully added\nplease wait a minute before login (for key distribution)\n";
	}
	else
	{
		die("Error adding access on '$site'. Aborting.\n");	
	}
}
	//add access
	$resp = strtolower(prompt("You do have access to $site. Login? (y/n)"));	
	if ($resp != 'y') die("ok, bye\n");
	$sshClient = isset($argv[2]) ? $argv[2] : null;
	if ($sshClient=='krusader')
	{
		$cmd = 'krusader --left sftp://'.$accaunts[1];
	}
	elseif($sshClient)
	{		
		$cmd = $sshClient.' sftp://'.$accaunts[1];
	}
	else
	{
		$cmd = 'ssh '.$accaunts[1];
	}
	echo "$cmd\n";
	passthru($cmd);
	//login

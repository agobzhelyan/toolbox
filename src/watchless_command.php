<?php
require_once(__DIR__.'/functions.php');

$output = trim(`which lessc`);
if (empty($output)) {
	writeln('You should have lessc command installed to use this tool.');
	die;
}

$output = trim(`which inotifywait`);
if (empty($output)) {
	writeln('You should have inotifywait command installed to use this tool.');
	writeln('You can install it by running:');
	writeln('  sudo apt-get install inotify-tools');
	die;
}

$lessPath = '';
while(!$lessPath) {
	$answer = trim(prompt('What\'s the input .less file?'));
	if ($answer) {
		$path = getAbsPath($answer);
		if (file_exists($path)) {
			$lessPath = $path;
			break;
		}
	}
	writeln('Sorry, can not find such file, try again.');
}
$lessPathEsc = escapeshellarg($lessPath);

$cssPath = '';
while(!$cssPath) {
	$answer = prompt('What\'s the ouput .css file?');
	$path = getAbsPath($answer);
	if (!file_exists($path)) {
		$createRes = @file_put_contents($path, '');
		if (false === $createRes) {
			writeln('Sorry, can not create output file %s', $path);
			continue;
		}
	}
	$cssPath = $path;
}
$cssPathEsc = escapeshellarg($cssPath);

$answer = prompt('What\'s the directory to watch for changes?');
$lessDir = getAbsPath($answer);
$lessDirEsc = escapeshellarg($lessDir);

writeln('Ok, starting to watch. Press Ctrl+C to terminate');
while(true) {
	$cmdOut = trim(`inotifywait -rq -e modify -e delete -e create --format "%w%f;%e" $lessDirEsc $lessPathEsc`);
	writeln($cmdOut);
	$lesscOut = `lessc $lessPathEsc > $cssPathEsc`;
}
//inotifywait -rq -e modify -e delete -e create --format "%w%f;%e" .
//./asdf;CREATE

function getAbsPath($path) {
	if ('/'===substr($path, 0, 1)) return $path;
	return getcwd().'/'.$path;
}
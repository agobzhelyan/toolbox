<?php
// usage: toolbox mount vickru
// or toolbox mount
require_once(__DIR__.'/functions.php');
$defaultPresets = loadPreset('mount', 'default');
$defaultBaseDir = empty($defaultPresets['baseDir']) ? null : $defaultPresets['baseDir'];
if (count($argv) < 2) {
	$baseDir = prompt('What is the base directory for mounted folders?' . ($defaultBaseDir ? " [$defaultBaseDir]":''));
	if (empty($baseDir)) $baseDir = $defaultBaseDir;
	if (!is_dir($baseDir)) {
		writeln("The path specified is not a directory: %s", $baseDir);
		die;
	}
	$defaultPresets['baseDir'] = $baseDir;
	savePreset('mount', 'default', $defaultPresets);

	$sshCredentials = prompt('SSH access credentials (username@somehost.com)');
	$folderName = prompt(sprintf('Mount folder [%s]', $sshCredentials));
	if (empty($folderName)) $folderName = $sshCredentials;
	$mountPath = rtrim($baseDir, '/') . '/'. $folderName;
	if (!is_dir($mountPath)) mkdir($mountPath);
	$command = sprintf('sshfs %s: %s', $sshCredentials, $mountPath);
	echo `$command`;
	writeln('Done.');
}
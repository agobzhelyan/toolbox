<?php
// put toolbox.phar somewhere current user may access it (to update w/o SU) 
// Create symlink in /usr/local/bin - requires SU
require_once(__DIR__.'/functions.php');
requireRootUser();
// create symlink in /usr/local/bin
$symlink = '/usr/local/bin/toolbox';

if (!preg_match('/\.phar$/', $mainScript)) {
	writeln('This command can be invoked only using toolbox.phar file, e.g.:');
	writeln('  sudo ./toolbox.phar install');
	die;
}
// handle the case when symlink (or binary) already exists
$linkDestination = @readlink($symlink);
if (file_exists($symlink) || $linkDestination) {
	// toolbox in bin folder is a link
	if ($linkDestination) {
		// if link points not to .phar file - ask user for confirmation
		if (!preg_match('/\.phar$/', $linkDestination)){
			$answer = prompt(sprintf('Symlink %s already exists and points to %s. Overewrite it? (y/N) [N]', 
				$symlink, $linkDestination 
			));
			if ('y' !== strtolower($answer)) die;
		}
	// toolbox in bin folder isn't a link
	} else {
		$answer = prompt(sprintf('File %s already exists. Overewrite it? (y/N) [N]', 
			$symlink
		));
		if ('y' !== strtolower($answer)) die;
	}
	unlink($symlink);
}

if ('/' === substr($mainScript, 0, 1)) {
	$pharPath = $mainScript;
} else {
	$pharPath = realpath(getcwd().'/'.$mainScript);
}
$command = sprintf('ln -s %s /usr/local/bin/toolbox', escapeshellarg($pharPath));
writeln('Creating symlink in /usr/local/bin ...');
echo `$command`;
writeln('Done. Now you can use "toolbox" command.');
<?php
// grab database dump over ssh (access by key assumed) and optionally import it locally
// toolbox getdb
// toolbox getdb -pl (preset list)
// toolbox getdb dkmega dkmega_new (use preset dkmega, import into dkmega_new database)
// ssh root@46.254.18.198 "mysqldump -udkmega -p**** dkmega|gzip" |gunzip |mysql -u insign -pinsign99 dkmega
require_once(__DIR__.'/functions.php');
writeln('Note: This command assumes you have ssh access by key to the remote server.');

$commandLine = implode(' ', $argv);
if (preg_match('/-p ?([^\s]+)/', $commandLine, $matches)) {
	$preset = $matches[1];
	$presetData = loadPreset('getdb', $preset);
	if (!empty($presetData)) {
		writeln('Loaded preset:');
		showPreset($presetData);
		extract($presetData);
	} else {
		writeln('Sorry, no %s preset found.', $preset);
	}
}
if (empty($presetData)) {
	$sshCredentials = prompt('SSH access credentials (username@myhost.com)');
	$dbName = prompt('Remote database name');
	$dbUser = prompt('Remote database user');
	$dbPass = prompt(sprintf('Remote database password for user %s', $dbUser), true);
	writeln('');	
}


$command = sprintf('ssh %s "mysqldump -u%s -p%s %s|gzip"', 
	$sshCredentials,
	$dbUser,
	$dbPass,
	$dbName
);

$answer = prompt('Import the data into local database? Y/n [Y]');
if ('n' === strtolower($answer)) {
	// just put to file
	$cwd = getcwd();
	$dumpFile = $cwd . '/'.$dbName.'.gz';
	writeln(sprintf('Database data will be put into %s', $dumpFile));
	$answer = strtolower(prompt('Ok? (Y/n) [Y]'));
	if ('n' === $answer) {
		$dumpFile = prompt('Ok, where should we put the dump?');
		if (0 !== strpos($dumpFile, '/')) {
			$dumpFile = $cwd . '/' . $dumpFile;
		}
	}
	$dumpFileShellEscaped = escapeshellarg($dumpFile);
	writeln(`$command > $dumpFileShellEscaped`);
	writeln('Done.');
	$answer = strtolower(prompt('Save remote server and database credentials as a preset? (Y/n) [Y]'));
	if ('n' !== $answer) {
		$presetName = prompt(sprintf('Preset name [%s]', $dbName));
		if (empty($presetName)) $presetName = $dbName;
		$data = array(
			'sshCredentials' => $sshCredentials,
			'dbUser' => $dbUser,
			'dbPass' => $dbPass,
			'dbName' => $dbName
		);
		savePreset('getdb', $presetName, $data);
	}
	die;
}
// ask data needed to import data locally